# :beginner: Physics From Finance Notes

## :speech_balloon: Background
These are my personal study notes from reading [Physics From Finance](https://www.amazon.com/Physics-Finance-introduction-fundamental-interactions/dp/1795882417).

## :memo: Finance vs. Physics

| :money_with_wings: Finance | :milky_way: Physics                        |
| :------------------------  | :----------------------------------------- |
| local currency             | gauge symmetry                             |
| rescaling currency         | gauge transformation                       |
| prices                     | gauge dependent quantity in internal space |
| countries                  | points in spacetime                        |
| opportunity to make money  | gain factor                                |
| currency arbitrage         | non-zero curvature in internal space       |
| banks modifying rates      | dynamical curvature                        |
| goods                      | electric charge                            |

* :speech_balloon: gauge invariant: a quantity that doesn't depend on local conventions
    * :currency_exchange: exchange rates are not gauge invariant (but arbitrage is)

## :chart: The Toy Financial Model
* [1 | Currency Rescaling](currency-rescaling.md)
* [2 | Arbitrage Gain Factor](gain-factor.md)
* [3 | Profitability & Current](profitability-current.md)
* [4 | Continuity](continuity.md)
