## :chart: The Toy Financial Model
### 4 | Continuity

* :exclamation: Big Idea: A *continuity equation* states that a quantity can neither be created nor destroyed (:recycle: *conservation law*) nor can it teleport &mdash; it can only move by a continuous flow.
    * continuity of goods in our toy financial model states that the change in the amount of goods in a country over time is equal to the net flow of goods in and out of the country in that time.

1. change of goods: $`J_0(\overrightarrow{n}+\overrightarrow{e_0})-J_0(\overrightarrow{n})`$
2. :arrow_up: and :arrow_right: flow: $`\displaystyle\sum_{i=1}^2 J_i(\overrightarrow{n})`$
3. :arrow_down: and :arrow_left: flow: $`\displaystyle-\sum_{i=1}^2 J_i(\overrightarrow{n}-\overrightarrow{e_i})`$
    * We need a minus sign for these flows because we are measuring from the neighboring countries
4. continuity equation: $`\displaystyle J_0(\overrightarrow{n}+\overrightarrow{e_0})-J_0(\overrightarrow{n}) = -\left(\sum_{i=1}^2 J_i(\overrightarrow{n})-\sum_{i=1}^2 J_i(\overrightarrow{n}-\overrightarrow{e_i})\right)`$
    * We flip the sign of our net flow because our :zap: current equation is treating outflow as positive
5. introduce *lattice spacing* $`a`$ such that we can take the *continuum limit* $`\displaystyle\lim_{a\to0}`$
    * :exclamation: Big Idea: infinitely many countries (spacetime is continuous)
    1. $`\displaystyle aq\tilde{H_0}(\overrightarrow{x}+a\overrightarrow{e_0})-aq\tilde{H_0}(\overrightarrow{x}) = -\left(\sum_{i=1}^2 aq\tilde{H_i}(\overrightarrow{x})-\sum_{i=1}^2 aq\tilde{H_i}(\overrightarrow{x}-a\overrightarrow{e_i})\right)`$
    2. $`\displaystyle q\left(\tilde{H_0}(\overrightarrow{x}+a\overrightarrow{e_0})-\tilde{H_0}(\overrightarrow{x})\right) = -q\left(\sum_{i=1}^2 \tilde{H_i}(\overrightarrow{x})-\sum_{i=1}^2 \tilde{H_i}(\overrightarrow{x}-a\overrightarrow{e_i})\right)`$
    3. $`q\left(\tilde{H_0}(\overrightarrow{x}+a\overrightarrow{e_0})-\tilde{H_0}(\overrightarrow{x})\right) = -q\left(\tilde{H_1}(\overrightarrow{x})-\tilde{H_1}(\overrightarrow{x}-a\overrightarrow{e_1})\right)-q\left(\tilde{H_2}(\overrightarrow{x})-\tilde{H_2}(\overrightarrow{x}-a\overrightarrow{e_2})\right)`$
        * :speech_balloon: Using $`-(a+b) = -a-b`$
    4. $`q\frac{\tilde{H_0}(\overrightarrow{x}+a\overrightarrow{e_0})-\tilde{H_0}(\overrightarrow{x})}{a} = -q\frac{\tilde{H_1}(\overrightarrow{x})-\tilde{H_1}(\overrightarrow{x}-a\overrightarrow{e_1})}{a}-q\frac{\tilde{H_2}(\overrightarrow{x})-\tilde{H_2}(\overrightarrow{x}-a\overrightarrow{e_2})}{a}`$
    5. $`q\frac{\partial\tilde{H_0}}{\partial x_0} = -q\frac{\partial\tilde{H_1}}{\partial x_1}-q\frac{\partial\tilde{H_2}}{\partial x_2}`$
        * :speech_balloon: Using $`\displaystyle f'(x) = \lim_{h \to 0} \frac{f(x)-f(x-h)}{h}`$
    6. $`\frac{\partial J_0}{\partial x_0} = -\frac{\partial J_1}{\partial x_1}-\frac{\partial J_2}{\partial x_2}`$
    7. $`\displaystyle\sum_{\mu=0}^2 \frac{\partial J_\mu}{\partial x_\mu} = 0`$
    8. $`\frac{\partial J_\mu}{\partial x_\mu} = 0`$
        * :thought_balloon: reminder: [Einstein Notation](https://en.wikipedia.org/wiki/Einstein_notation)
