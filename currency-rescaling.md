## :chart: The Toy Financial Model
### 1 | Currency Rescaling
1. :globe_with_meridians: start with a 2D lattice (grid)
2. identify countries :japan: with vectors :arrow_upper_right: (points where grid lines cross)
3. reference neighboring countries by adding/subtracting basis vectors
    * see: [Euclidean Vector](https://en.wikipedia.org/wiki/Euclidean_vector), [Einstein Notation](https://en.wikipedia.org/wiki/Einstein_notation)
4. exchange rates between $`\overrightarrow{n}`$ and *i*-direction neighbor denoted $`R_i(\overrightarrow{n})`$
5. natural log of exchange rate $`R_i(\overrightarrow{n})`$ denoted $`A_i(\overrightarrow{n})`$
6. rescaling currency of country $`\overrightarrow{n}`$ by a factor *f* denoted $`f(\overrightarrow{n})`$
7. natural log of $`f(\overrightarrow{n})`$ denoted $`\epsilon(\overrightarrow{n})`$
    * :speech_balloon: $`\epsilon`$ is an epsilon
8. currency rescaling equation: $`R_i(\overrightarrow{n}) \rightarrow \frac{f(\overrightarrow{n}+\overrightarrow{e_i})}{f(\overrightarrow{n})}R_i(\overrightarrow{n})`$
    1. $`e^{A_i(\overrightarrow{n})} \rightarrow \frac{e^{\epsilon(\overrightarrow{n}+\overrightarrow{e_i})}}{e^{\epsilon(\overrightarrow{n})}}e^{A_i(\overrightarrow{n})}`$
        * :speech_balloon: Using $`e^{\ln{x}} = x`$
    2. $`e^{A_i(\overrightarrow{n})} \rightarrow e^{A_i(\overrightarrow{n})+\epsilon(\overrightarrow{n}+\overrightarrow{e_i})-\epsilon(\overrightarrow{n})}`$
        * :speech_balloon: Using $`\frac{e^A}{e^B} = e^{A-B}`$, $`e^A e^B = e^{A+B}`$
    3. $`A_i(\overrightarrow{n}) \rightarrow A_i(\overrightarrow{n})+\epsilon(\overrightarrow{n}+\overrightarrow{e_i})-\epsilon(\overrightarrow{n})`$
9. :hourglass: introduce time as zeroth-coordinate using $`\mu,\nu \in \{0,1,2\}`$ instead of $`i,j \in \{1,2\}`$
    * :speech_balloon: $`\mu`$ is a mu, $`\nu`$ is a nu
    * :exclamation: Big Idea: package time with space into *spacetime*
    1. rescaling equation becomes: $`A_\mu(\overrightarrow{n}) \rightarrow A_\mu(\overrightarrow{n})+\epsilon(\overrightarrow{n}+\overrightarrow{e_\mu})-\epsilon(\overrightarrow{n})`$
10. introduce *lattice spacing* $`a`$ such that we can take the *continuum limit* $`\displaystyle\lim_{a \to 0}`$
    * :exclamation: Big Idea: infinitely many countries (spacetime is continuous)
    1. introduce $`\overrightarrow{x} = a\overrightarrow{n}`$ where $`\displaystyle\lim_{a \to 0} \overrightarrow{n} \to \infty`$, keeping $`\overrightarrow{x}`$ fixed to the value $`\overrightarrow{n}`$ had before we took the limit
        * :triangular_ruler: What we are doing is introducing a measure of distance independent of the lattice. Before taking the limit, treat the lattice like a 2d measuring stick where each line from the origin along an axis is 1 unit. Now, if you decrease the lattice spacing by half, you have twice as many lines to reach the same distance as measured by the original lattice. We are using the original lattice as our ruler to measure distance over the continuum.
    2. introduce $`a\tilde{A_\mu}(\overrightarrow{x}) = A_\mu(\overrightarrow{n})`$
        * :triangular_ruler: We are defining $`a\tilde{A_\mu}(\overrightarrow{x})`$ to be our ruler for $`A_\mu(\overrightarrow{n})`$ in the same way that $`\overrightarrow{x}`$ is our ruler for $`\overrightarrow{n}`$. The extra factor $`a`$ is necessary to get fixed finite quantities out of our equations when we take the limit.
    3. rescaling equation becomes: $`a\tilde{A_\mu}(\overrightarrow{x}) \rightarrow a\tilde{A_\mu}(\overrightarrow{x})+\epsilon(\overrightarrow{x}+a\overrightarrow{e_\mu})-\epsilon(\overrightarrow{x})`$
        1. $`\tilde{A_\mu}(\overrightarrow{x}) \rightarrow \tilde{A_\mu}(\overrightarrow{x})+\frac{\epsilon(\overrightarrow{x}+a\overrightarrow{e_\mu})-\epsilon(\overrightarrow{x})}{a}`$
            * :speech_balloon: Using $`\frac{a}{a} = 1`$
        2. $`\tilde{A_\mu}(\overrightarrow{x}) \rightarrow \tilde{A_\mu}(\overrightarrow{x})+\frac{\partial\epsilon}{\partial x_\mu}`$
            * :speech_balloon: Using definition of partial derivative
            * see: [Derivative](https://en.wikipedia.org/wiki/Derivative#Rigorous_definition), [Partial Derivative](https://en.wikipedia.org/wiki/Partial_derivative#Formal_definition)
            * The partial derivative is with respect to $`x_\mu`$ because $`\overrightarrow{x}+a\overrightarrow{e_\mu}`$ adds $`a`$ to the $`x_\mu`$ component of $`\overrightarrow{x}`$
    4. we drop the tildes to unclutter the notation
        1. rescaling equation becomes: $`A_\mu(\overrightarrow{x}) \rightarrow A_\mu(\overrightarrow{x})+\frac{\partial\epsilon}{\partial x_\mu}`$
