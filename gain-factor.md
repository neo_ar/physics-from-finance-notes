## :chart: The Toy Financial Model
### 2 | Arbitrage Gain Factor

1. gain factor equation: $`G(\overrightarrow{n}) = R_i(\overrightarrow{n})R_j(\overrightarrow{n}+\overrightarrow{e_i})\frac{1}{R_i(\overrightarrow{n}+\overrightarrow{e_j})}\frac{1}{R_j(\overrightarrow{n})}`$
    * good practice understanding the vector index notation
        * indices control what loop we follow in this equation
    * $`G > 1`$ means we make money :chart_with_upwards_trend:, $`G < 1`$ means we lose money :chart_with_downwards_trend:
        * :question: $`1-G`$ = measure of curvature?
2. natural log of $`G(\overrightarrow{n})`$ denoted $`F_{ij}(\overrightarrow{n})`$
    1. $`e^{F_{ij}(\overrightarrow{n})} = e^{A_i(\overrightarrow{n})}e^{A_j(\overrightarrow{n}+\overrightarrow{e_i})}\frac{1}{e^{A_i(\overrightarrow{n}+\overrightarrow{e_j})}}\frac{1}{e^{A_j(\overrightarrow{n})}}`$
        * :speech_balloon: Using $`e^{\ln{x}} = x`$
    2. $`e^{F_{ij}(\overrightarrow{n})} = e^{A_i(\overrightarrow{n})+A_j(\overrightarrow{n}+\overrightarrow{e_i})-A_i(\overrightarrow{n}+\overrightarrow{e_j})-A_j(\overrightarrow{n})}`$
        * :speech_balloon: Using $`\frac{e^A}{e^B} = e^{A-B}`$, $`e^A e^B = e^{A+B}`$
    3. $`F_{ij}(\overrightarrow{n}) = A_i(\overrightarrow{n})+A_j(\overrightarrow{n}+\overrightarrow{e_i})-A_i(\overrightarrow{n}+\overrightarrow{e_j})-A_j(\overrightarrow{n})`$
    4. $`F_{ij}(\overrightarrow{n}) = A_j(\overrightarrow{n}+\overrightarrow{e_i})-A_j(\overrightarrow{n})-[A_i(\overrightarrow{n}+\overrightarrow{e_j})-A_i(\overrightarrow{n})]`$
        * :speech_balloon: Using $`a-b = -(b-a)`$
        * :speech_balloon: *Antisymmetry*: $`F_{ij}(\overrightarrow{n}) = -F_{ji}(\overrightarrow{n})`$
        * :exclamation: Big Idea: possibility of arbitrage is what is physical in the structure of exchange rates
3. :hourglass: introduce time as zeroth-coordinate using $`\mu,\nu \in \{0,1,2\}`$ instead of $`i,j \in \{1,2\}`$
    * :speech_balloon: $`\mu`$ is a mu, $`\nu`$ is a nu
    * :exclamation: Big Idea: package time with space into *spacetime*
    1. gain factor equation becomes: $`F_{\mu\nu}(\overrightarrow{n}) = A_\nu(\overrightarrow{n}+\overrightarrow{e_\mu})-A_\nu(\overrightarrow{n})-[A_\mu(\overrightarrow{n}+\overrightarrow{e_\nu})-A_\mu(\overrightarrow{n})]`$
4. introduce *lattice spacing* $`a`$ such that we can take the *continuum limit* $`\displaystyle\lim_{a\to0}`$
    * :exclamation: Big Idea: infinitely many countries (spacetime is continuous)
    1. gain factor equation becomes: $`F_{\mu\nu}(\overrightarrow{n}) = a\tilde{A_\nu}(\overrightarrow{x}+a\overrightarrow{e_\mu})-a\tilde{A_\nu}(\overrightarrow{x})-[a\tilde{A_\mu}(\overrightarrow{x}+a\overrightarrow{e_\nu})-a\tilde{A_\mu}(\overrightarrow{x})]`$
        1. $`F_{\mu\nu}(\overrightarrow{n}) = a^2\frac{\tilde{A_\nu}(\overrightarrow{x}+a\overrightarrow{e_\mu})-\tilde{A_\nu}(\overrightarrow{x})}{a}-a^2\frac{\tilde{A_\mu}(\overrightarrow{x}+a\overrightarrow{e_\nu})-\tilde{A_\mu}(\overrightarrow{x})}{a}`$
            * :speech_balloon: Using $`\frac{a}{a} = 1`$
        2. introduce $`a^2\tilde{F_{\mu\nu}}(\overrightarrow{x}) = F_{\mu\nu}(\overrightarrow{n})`$
            * :triangular_ruler: We are defining $`a^2\tilde{F_{\mu\nu}}(\overrightarrow{x})`$ to be our ruler for $`F_{\mu\nu}(\overrightarrow{n})`$ in the same way that $`\overrightarrow{x}`$ is our ruler for $`\overrightarrow{n}`$. The extra factor $`a^2`$ is necessary to get fixed finite quantities out of our equations when we take the limit.
            * $`a^2\tilde{F_{\mu\nu}}(\overrightarrow{x}) = a^2\frac{\tilde{A_\nu}(\overrightarrow{x}+a\overrightarrow{e_\mu})-\tilde{A_\nu}(\overrightarrow{x})}{a}-a^2\frac{\tilde{A_\mu}(\overrightarrow{x}+a\overrightarrow{e_\nu})-\tilde{A_\mu}(\overrightarrow{x})}{a}`$
        3. $`\tilde{F_{\mu\nu}}(\overrightarrow{x}) = \frac{\tilde{A_\nu}(\overrightarrow{x}+a\overrightarrow{e_\mu})-\tilde{A_\nu}(\overrightarrow{x})}{a}-\frac{\tilde{A_\mu}(\overrightarrow{x}+a\overrightarrow{e_\nu})-\tilde{A_\mu}(\overrightarrow{x})}{a}`$
        4. $`\tilde{F_{\mu\nu}}(\overrightarrow{x}) = \frac{\partial\tilde{A_\nu}}{\partial x_\mu}-\frac{\partial\tilde{A_\mu}}{\partial x_\nu}`$
            * :speech_balloon: Using definition of partial derivative
    2. we drop the tildes to unclutter the notation
        1. gain factor equation becomes: $`F_{\mu\nu}(\overrightarrow{x}) = \frac{\partial A_\nu}{\partial x_\mu}-\frac{\partial A_\mu}{\partial x_\nu}`$
