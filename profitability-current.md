## :chart: The Toy Financial Model
### 3 | Profitability & Current

1. :package: introduce *goods* that can be *sold*
2. price of good at country $`\overrightarrow{n}`$ denoted $`p(\overrightarrow{n})`$
3. profitability equation: $`g_i(\overrightarrow{n}) = \frac{p(\overrightarrow{n}+\overrightarrow{e_i})}{p(\overrightarrow{n})R_i(\overrightarrow{n})}`$
    * This equation tells us how lucrative it will be to sell a good from one country in a neighboring country.
    * :thought_balloon: reminder: $`R_i(\overrightarrow{n})`$ denotes the exchange rate
    * $`g_i > 1`$ means we make money :chart_with_upwards_trend:, $`g_i < 1`$ means we lose money :chart_with_downwards_trend:
4. natural log of price $`p(\overrightarrow{n})`$ denoted $`\phi(\overrightarrow{n})`$
    * :speech_balloon: $`\phi`$ is a phi
5. natural log of profitability $`g_i(\overrightarrow{n})`$ denoted $`H_i(\overrightarrow{n})`$
6. $`e^{H_i(\overrightarrow{n})} = \frac{e^{\phi(\overrightarrow{n}+\overrightarrow{e_i})}}{e^{\phi(\overrightarrow{n})}e^{A_i(\overrightarrow{n})}}`$
    * :speech_balloon: Using $`e^{\ln{x}} = x`$
7. $`e^{H_i(\overrightarrow{n})} = e^{\phi(\overrightarrow{n}+\overrightarrow{e_i})-(\phi(\overrightarrow{n})+A_i(\overrightarrow{n}))}`$
    * :speech_balloon: Using $`\frac{e^A}{e^B} = e^{A-B}`$, $`e^A e^B = e^{A+B}`$
8. $`e^{H_i(\overrightarrow{n})} = e^{\phi(\overrightarrow{n}+\overrightarrow{e_i})-\phi(\overrightarrow{n})-A_i(\overrightarrow{n})}`$
    * :speech_balloon: Using $`-a-b = -(a+b)`$
9. $`H_i(\overrightarrow{n}) = \phi(\overrightarrow{n}+\overrightarrow{e_i})-\phi(\overrightarrow{n})-A_i(\overrightarrow{n})`$
10. :zap: current equation: $`J_i(\overrightarrow{n}) = qH_i(\overrightarrow{n})`$ where $`q`$ indicates how strongly traders react to an opportunity to earn money trading a good
    * This equation measures the amount of goods flowing between countries.
11. :hourglass: introduce time as zeroth-coordinate using $`\mu,\nu \in \{0,1,2\}`$ instead of $`i,j \in \{1,2\}`$
    * :speech_balloon: $`\mu`$ is a mu, $`\nu`$ is a nu
    * :exclamation: Big Idea: package time with space into *spacetime*
    1. profitability equation becomes: $`H_\mu(\overrightarrow{n}) = \phi(\overrightarrow{n}+\overrightarrow{e_\mu})-\phi(\overrightarrow{n})-A_\mu(\overrightarrow{n})`$
    2. current equation becomes: $`J_\mu(\overrightarrow{n}) = qH_\mu(\overrightarrow{n})`$
12. introduce *lattice spacing* $`a`$ such that we can take the *continuum limit* $`\displaystyle\lim_{a\to0}`$
    * :exclamation: Big Idea: infinitely many countries (spacetime is continuous)
    1. introduce $`\tilde{\phi}(\overrightarrow{x}) = \phi(\overrightarrow{n})`$
        * :triangular_ruler: We are defining $`\tilde{\phi}(\overrightarrow{x})`$ to be our ruler for $`\phi(\overrightarrow{n})`$ in the same way that $`\overrightarrow{x}`$ is our ruler for $`\overrightarrow{n}`$.
    2. profitability equation becomes: $`H_\mu(\overrightarrow{n}) = \tilde{\phi}(\overrightarrow{x}+a\overrightarrow{e_\mu})-\tilde{\phi}(\overrightarrow{x})-a\tilde{A_\mu}(\overrightarrow{x})`$
        1. introduce $`a\tilde{H_\mu}(\overrightarrow{x}) = H_\mu(\overrightarrow{n})`$
            * :triangular_ruler: We are defining $`a\tilde{H_\mu}(\overrightarrow{x})`$ to be our ruler for $`H_\mu(\overrightarrow{n})`$ in the same way that $`\overrightarrow{x}`$ is our ruler for $`\overrightarrow{n}`$. The extra factor $`a`$ is necessary to get fixed finite quantities out of our equations when we take the limit.
        2. $`\tilde{H_\mu}(\overrightarrow{x}) = \frac{\tilde{\phi}(\overrightarrow{x}+a\overrightarrow{e_\mu})-\tilde{\phi}(\overrightarrow{x})}{a}-\tilde{A_\mu}(\overrightarrow{x})`$
        3. $`\tilde{H_\mu}(\overrightarrow{x}) = \frac{\partial\tilde{\phi}}{\partial x_\mu}-\tilde{A_\mu}(\overrightarrow{x})`$
            * :speech_balloon: Using definition of partial derivative
    3. current equation becomes: $`J_\mu(\overrightarrow{x}) = q\left(\frac{\partial\tilde{\phi}}{\partial x_\mu}-\tilde{A_\mu}(\overrightarrow{x})\right)`$
    4. we drop the tildes to unclutter the notation
        1. profitability equation becomes: $`H_\mu(\overrightarrow{x}) = \frac{\partial\phi}{\partial x_\mu}-A_\mu(\overrightarrow{x})`$
            * $`H_0(\overrightarrow{x})`$ measures the change in the price of a good in a country over time.
        2. current equation becomes: $`J_\mu(\overrightarrow{x}) = q\left(\frac{\partial\phi}{\partial x_\mu}-A_\mu(\overrightarrow{x})\right)`$
            * $`J_0(\overrightarrow{x})`$ measures the amount of a good in a country over time.
